public class FindMin {
    public static void main(String[] args) {
        if (args.length == 3) {
            int value1 = Integer.parseInt(args[0]);
            int value2 = Integer.parseInt(args[1]);
            int value3 = Integer.parseInt(args[2]);

            int smallestNumber = value3;

            if (value2 >= value1 && value3 >= value1) {
                smallestNumber = value1;
            } else if (value1 >= value2 && value3 >= value2) {
                smallestNumber = value2;
            }

            System.out.println(smallestNumber);
        } else {
            System.out.println("Must be entered 3 numbers in order to find the smallest one!");
        }
    }
}