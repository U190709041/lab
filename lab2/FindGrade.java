public class FindGrade {
    public static void main(String[] args) {
        if (args.length >= 1) {
            int score = Integer.parseInt(args[0]);

            if (score >= 0 && score <= 100) {
                String letterGrade;

                if (score >= 90) {
                    letterGrade = "A";
                } else if (score >= 80) {
                    letterGrade = "B";
                } else if (score >= 70) {
                    letterGrade = "C";
                } else if (score >= 60) {
                    letterGrade = "D";
                } else {
                    letterGrade = "F";
                }

                System.out.println("Your grade is " + letterGrade);
            } else {
                System.out.println("It is not a valid score!");
            }
        } else {
            System.out.println("Should be entered a score to find the letter grade!");
        }
    }
}
