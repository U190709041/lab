public class GCDLoop {
    public static void main(String[] args) {
        if (args.length < 2 || args.length > 3) {
            System.out.print("Should be given exactly 2 numbers to find the greatest common divisor using Loop.");
        } else {
            int number1 = Integer.parseInt(args[0]);
            int number2 = Integer.parseInt(args[1]);

            int bigNumber = number1 > number2 ? number1 : number2; // Big number
            int q = bigNumber == number1 ? number2 : number1; // Quotient
            int r; // Remainder

            while(true) {
                r = bigNumber % q;

                // Check number1 and number2 until remainder equals to 0
                if (r == 0) {
                    System.out.println(q);
                    break;
                } else {
                    q = r;
                }
            }
        }
    }
}
