public class GCDRec {
    public static void main(String[] args) {
        if (args.length < 2 || args.length > 3) {
            System.out.print("Should be given exactly 2 numbers to find the greatest common divisor using Recursion.");
        } else {
            int num1 = Integer.parseInt(args[0]);
            int num2 = Integer.parseInt(args[1]);

            int bigNumber = num1 > num2 ? num1 : num2;
            int smallNumber = bigNumber == num1 ? num2 : num1;

            System.out.println(GCDRec(bigNumber, smallNumber));
        }
    }

    public static int GCDRec(int number1, int number2) {
        if (number2 != 0)
            return GCDRec(number2, number1 % number2);
        else
            return number1;
    }
}
