import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();

		boolean found = false;
		search: for (int i=0; i< matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				if (search(0, matrix, i, j)){
					found = true;
					break search;
				}
			}
		}
			
		if (found) {
			System.out.println("A sequence is found");
		}
		printMatrix(matrix);
	}

	private static boolean search (int number, int[][]matrix, int row, int col) {
		if (number >= 0 && number < 9 && matrix[row][col] == number) {
			int searchNumber = number + 1;

			boolean up = row > 0 ? search(searchNumber, matrix, row - 1, col) : false;
			boolean right = col < 9 ? search(searchNumber, matrix, row, col + 1) : false;
			boolean down = row < 9 ? search(searchNumber, matrix, row + 1, col) : false;
			boolean left = col > 0 ? search(searchNumber, matrix, row, col - 1) : false;

			if (up || right || down || left) {
				matrix[row][col] = 9;

				if (up) matrix[row - 1][col] = 9 - searchNumber;
				if (right) matrix[row][col + 1] = 9 - searchNumber;
				if (down) matrix[row + 1][col] = 9 - searchNumber;
				if (left) matrix[row][col - 1] = 9 - searchNumber;

				return true;
			} else {
				return false;
			}
		} else if (number == 9 && matrix[row][col] == number) {
			return true;
		}

		return false;
	}

	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("lab5/matrix.txt"); // if you run from command line use  new File("matrix.txt") instead

		try (Scanner sc = new Scanner(file)){
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
