public class MyDate {
    private int day, month, year;

    int[] maxDayOfMonths = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public String toString() {
        return year + "-" + (month < 9 ? "0" : "") + (month + 1) + "-" + (day < 10 ? "0" : "") + day;
    }

    private boolean isLeapYear() {
        return this.year % 4 == 0;
    }

    private void controlLeapYear() {
        if (this.isLeapYear()) {
            if (this.maxDayOfMonths[1] != 29) {
                this.maxDayOfMonths[1] = 29;
            }
        } else if (this.maxDayOfMonths[1] != 28) {
            this.maxDayOfMonths[1] = 28;
        }
    }

    public void incrementDay() {
        this.controlLeapYear();

        if (this.day + 1 > this.maxDayOfMonths[this.month]) {
            this.day = 1;

            if (this.month + 1 > this.maxDayOfMonths.length - 1) {
                this.month = 0;
                this.year += 1;
            } else {
                this.month += 1;
            }
        } else {
            this.day += 1;
        }
    }

    public void incrementDay(int day) {
        while (day > 0) {
            this.incrementDay();
            day--;
        }
    }

    public void incrementYear() {
        this.year += 1;
        this.controlLeapYear();

        if (this.day > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void incrementYear(int year) {
        int currentDay = this.day;

        while (year > 0) {
            this.incrementYear();
            year--;
        }

        if (this.isLeapYear() && this.month == 1 && currentDay >= 29) {
            this.day = 29;
        } else if (currentDay > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void decrementDay() {
        this.controlLeapYear();

        if (this.day - 1 == 0) {
            if (this.month == 0) {
                this.day = this.maxDayOfMonths[this.maxDayOfMonths.length - 1];
                this.month = this.maxDayOfMonths.length - 1;
                this.year -= 1;
            } else {
                this.day = this.maxDayOfMonths[this.month - 1];
                this.month -= 1;
            }
        } else {
            this.day -= 1;
        }
    }

    public void decrementDay(int day) {
        while (day > 0) {
            this.decrementDay();
            day--;
        }
    }

    public void decrementYear() {
        this.year -= 1;
        this.controlLeapYear();

        if (this.day > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void decrementYear(int year) {
        int currentDay = this.day;

        while (year > 0) {
            this.decrementYear();
            year--;
        }

        if (this.isLeapYear() && this.month == 1 && currentDay >= 29) {
            this.day = 29;
        } else if (currentDay > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void decrementMonth() {
        if (this.month == 0) {
            this.month = this.maxDayOfMonths.length - 1;
            this.year -= 1;
            this.controlLeapYear();
        } else {
            this.month -= 1;
        }

        if (this.day > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void decrementMonth(int month) {
        int currentDay = this.day;

        while (month > 0) {
            this.decrementMonth();
            month--;
        }

        if (currentDay > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void incrementMonth() {
        if (this.month + 1 > this.maxDayOfMonths.length - 1) {
            this.month = 0;
            this.year += 1;
            this.controlLeapYear();
        } else {
            this.month += 1;
        }

        if (this.day > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public void incrementMonth(int month) {
        int currentDay = this.day;

        while (month > 0) {
            this.incrementMonth();
            month--;
        }

        if (currentDay > this.maxDayOfMonths[this.month]) {
            this.day = this.maxDayOfMonths[this.month];
        }
    }

    public boolean isBefore(MyDate anotherDate) {
        return !this.isAfter(anotherDate);
    }

    public boolean isAfter(MyDate anotherDate) {
        if ((this.year > anotherDate.year) || (this.year == anotherDate.year && (this.month > anotherDate.month || (this.month == anotherDate.month && this.day > anotherDate.day)))) {
            return true;
        } else {
            return false;
        }
    }

    public int dayDifference(MyDate anotherDate) {
        MyDate bigDate = this.isAfter(anotherDate) ? this : anotherDate;
        MyDate prevDate = bigDate == this ? anotherDate : this;

        int daysBetweenTwoDate = 0;
        int countYears = bigDate.year - prevDate.year,
            countMonths = bigDate.month - prevDate.month,
            countDays = bigDate.day - prevDate.day;

        if (countYears > 0) daysBetweenTwoDate += 365 * countYears;
        if (countDays > 0) daysBetweenTwoDate += countDays;

        if (countMonths > 0) {
            int currentMonth = bigDate.month - 1;

            while (currentMonth > 0) {
                daysBetweenTwoDate += this.maxDayOfMonths[currentMonth];
                currentMonth--;
            }
        }

        return daysBetweenTwoDate;
    }
}



