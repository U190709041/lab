package generics;

import java.util.List;

public interface Stack<T> {

    void push(T item);
    T pop();
    boolean empty();

    List<T> toList();

    default void addAll(Stack<T> aStack) {
        List<T> values = aStack.toList();
        for (T value : values) {
            push(value);
        }
    }
}
