package generics;

public class StackDemo {
    public static void main(String[] args) {

        Stack<Integer> stack = new StackImpl<>();
        stack.push(1);
        stack.push(5);
        stack.push(7);
        stack.push(9);
        stack.push(4);

        System.out.println(stack.toList());

        Stack<Integer> stackA = new StackImpl<>();
        stackA.push(3);
        stackA.push(2);
        stackA.push(1);

        System.out.println(stackA.toList());

        stack.addAll(stackA);
        System.out.println(stack.toList());

        int total = 0;
        while (!stack.empty())
            total += stack.pop();

        System.out.println("total = " + total);
        System.out.println(stack.toList());
    }
}
