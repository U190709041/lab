import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		// Icon for each player which will be printed to board, also
		// Helper with the length of array to understand how many player play this game
		char[] playerIcons = { 'X', 'O' };
		int currentPlayer = 1; // Current Player ID
		boolean isGameContinue = true; // Helper to stop the loop when game is finished

		printBoard(board);

		while (isGameContinue) {
			if (!isBoardFull(board)) {
				// Get row and col params from the player
				System.out.print("Player " + currentPlayer + " enter row number:");
				int row = reader.nextInt();
				System.out.print("Player " + currentPlayer + " enter column number:");
				int col = reader.nextInt();

				if (
						(row >= 1 && row <= board.length) &&
						(col >= 1 && col <= board.length) &&
						board[row - 1][col - 1] == ' '
				) {
					board[row - 1][col - 1] = playerIcons[currentPlayer - 1];
					printBoard(board);

					if (checkboard(board)) {
						System.out.print("Player " + currentPlayer + " is the winner.");
						isGameContinue = false;
					} else {
						// Setting the current player to the next player
						// If it was the last person, set the current player to first player
						if (playerIcons.length > currentPlayer)
							currentPlayer += 1;
						else
							currentPlayer = 1;
					}
				}
			} else {
				System.out.print("Game ended with a draw.");
				isGameContinue = false;
			}
		}

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			}
			System.out.println();
			System.out.println("   -----------");
		}
	}

	// Checks whether a win condition exists or not
	public static boolean checkboard(char[][] board) {
		// Loops through columns checking if any winning state exists
		for (int columns = 0; columns < 3; columns++) {
			if (board[0][columns] == board[1][columns] && board[1][columns] == board[2][columns] && board[2][columns] != ' ')
				return true;
		}
		// Loops through rows checking if any winning state exists
		for (int rows = 0; rows < 3; rows++) {
			if (board[rows][0] == board[rows][1] && board[rows][1] == board[rows][2] && board[rows][2] != ' ')
				return true;
		}
		// checks diagonals if any winning state exists
		if (
				(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[2][2] != ' ') ||
				(board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[2][0] != ' ')
		)
			return true;

		return false;
	}

	// Checks board if all rows and columns are empty
	public static boolean isBoardFull(char[][] board) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i][j] == ' ') {
					return false;
				}
			}
		}
		return true;
	}
}