import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number = rand.nextInt(100); //generates a number between 0 and 99

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		int guess = reader.nextInt(); //Read the user input
		boolean isContinue = true; //Helper for while loop
		int attempts = 0; //Counting how many times tried to guess

		while (isContinue) {
			if (guess == -1) {
				System.out.println("Sorry, the number was " + number);
				isContinue = false;
			} else if (guess == number) {
				System.out.println("Congratulations! You won after " + attempts + " attempts!");
				isContinue = false;
			} else {
				attempts += 1;
				System.out.println("Sorry!");

				if (guess < number) {
					System.out.println("Mine is greater than your guess.");
				} else if (guess > number) {
					System.out.println("Mine is less than your guess.");
				}

				System.out.print("Type -1 to quit or guess another: ");
				guess = reader.nextInt();
			}
		}

		reader.close(); //Close the resource before exiting
	}
}