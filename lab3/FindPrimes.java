public class FindPrimes {
    public static void main(String[] args) {
        if (args.length == 1) {
            int num = Integer.parseInt(args[0]); //Given number from user
            String primeNumbers = "";

            for (int i = 2; i <= num; i++) {
                boolean isPrime = true;

                for (int counter = 2; counter < i; counter++) {
                    if (i % counter == 0) {
                        isPrime = false;
                        break;
                    }
                }

                if (isPrime) {
                    primeNumbers = primeNumbers + i + ",";
                }
            }

            // Printing by removing last comma from the prime numbers string
            System.out.println(primeNumbers.substring(0 ,primeNumbers.length() - 1));
        } else {
            System.out.println("You should write a number to print the prime numbers up to the number.");
        }
    }
}